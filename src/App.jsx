import Navbar from "./components/Navbar";
import TodoApp from "./pages/TodoApp";
import About from "./pages/About";
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="wrapper">
      <Navbar />
      <Routes>
        <Route path="/" element={<About />} />
        <Route path="/about" element={<About />} />
        <Route path="/todo-app" element={<TodoApp />} />
      </Routes>
    </div>
  );
}

//Without react components -> entire page reloads
/* function App() {
    let component;

    switch (window.location.pathname) {
        case "/":
            component = <About />;
            break;
        case "/about":
            component = <About />;
            break;
        case "/todo-app":
            component = <TodoApp />;
            break;
    }

    return (
        <div className="wrapper">
            <Navbar />
            {component}
        </div>
    );
} */

export default App;
