const About = () => {
  return (
    <div className="container">
      <p>
        This is a simple todo app built to demonstrate how react works and how
        to build a simple application with states and routers
      </p>
    </div>
  );
};

export default About;
