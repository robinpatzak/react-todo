import Header from "../components/Header";
import TodoContainer from "../components/TodoContainer";
import AddTodo from "../components/AddTodo";
import { useState } from "react";

const TodoApp = () => {
  const [todos, setTodos] = useState([
    {
      id: 1,
      text: "Example Todo",
      urgent: false,
    },
  ]);

  const addTodo = (todo) => {
    const id = Math.floor(Math.random() * 10000) + 1;
    const newTodo = { id, ...todo };
    setTodos([...todos, newTodo]);
  };

  const deleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const toggleUrgent = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, urgent: !todo.urgent } : todo
      )
    );
  };

  return (
    <div className="container">
      <Header />
      <AddTodo onAdd={addTodo} />
      {todos.length > 0 ? (
        <TodoContainer
          todos={todos}
          onDelete={deleteTodo}
          onToggleUrgent={toggleUrgent}
        />
      ) : (
        <p style={{ color: "red" }}>Create some todos</p>
      )}
    </div>
  );
};

export default TodoApp;
