const Todo = ({ todo, onDelete, onToggleUrgent }) => {
  return (
    <div
      className={`todo ${todo.urgent ? "urgent" : ""}`}
      onDoubleClick={() => onToggleUrgent(todo.id)}
    >
      <h3>
        {todo.text}{" "}
        <p
          style={{ color: "red", cursor: "pointer" }}
          onClick={() => onDelete(todo.id)}
        >
          X
        </p>
      </h3>
    </div>
  );
};

export default Todo;
