import { useState } from "react";

const AddTodo = ({ onAdd }) => {
  const [text, setText] = useState("");
  const [urgent, setUrgent] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    if (!text) {
      alert("Text field may not be empty");
      return;
    }

    onAdd({ text, urgent });

    setText("");
    setUrgent(false);
  };

  return (
    <form className="add-form" onSubmit={onSubmit}>
      <div className="form-control">
        <input
          type="text"
          placeholder="Add todo"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <input
          type="checkbox"
          checked={urgent}
          value={urgent}
          onChange={(e) => setUrgent(e.currentTarget.checked)}
        />
        <input className="btn" type="submit" value="Add todo" />
      </div>
    </form>
  );
};

export default AddTodo;
