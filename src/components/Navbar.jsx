import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="nav">
      <ul>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/todo-app">Todo-App</Link>
        </li>
      </ul>
    </nav>
  );
};

//Without react components -> entire page reloads
/* const Navbar = () => {
    return (
        <nav className="nav">
            <ul>
                <li>
                    <a href="/about">About</a>
                </li>
                <li>
                    <a href="/todo-app">Todo-App</a>
                </li>
            </ul>
        </nav>
    );
}; */

export default Navbar;
