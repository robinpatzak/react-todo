import Todo from "./Todo";

const TodoContainer = ({ todos, onDelete, onToggleUrgent }) => {
  return (
    <>
      {todos.map((todo) => (
        <Todo
          key={todo.id}
          todo={todo}
          onDelete={onDelete}
          onToggleUrgent={onToggleUrgent}
        />
      ))}
    </>
  );
};

export default TodoContainer;
